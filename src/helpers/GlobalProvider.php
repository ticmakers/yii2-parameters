<?php

namespace ticmakers\parameters\helpers;

use Yii;

/**
 * Proveedor de datos por defecto para los elementos de los parámetros
 *
 * @package ticmakers
 * @subpackage parameters/helpers
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @version 0.0.1
 * @since 1.0.0
 */
class GlobalProvider
{
}