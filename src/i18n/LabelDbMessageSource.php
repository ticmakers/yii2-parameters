<?php

namespace ticmakers\parameters\i18n;

use ticmakers\core\i18n\LabelDbMessageSource as LabelDbMessageSourceCore;

/**
 *
 *
 * @author Juan Sebastian Muñoz Reyes <juan.munoz@ticmakers.com>
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @author Kevin Daniel Guzman Delgadillo <kevin.guzman@ticmakers.com>
 */

class LabelDbMessageSource extends LabelDbMessageSourceCore
{ }
