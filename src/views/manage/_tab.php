<?php
use yii\helpers\Html;
use kartik\select2\Select2;
use dosamigos\ckeditor\CKEditor;
use kartik\date\DatePicker;
use kartik\number\NumberControl;
use kartik\widgets\TimePicker;
use kartik\widgets\DateTimePicker;

$moduleInstance = Yii::$app->getModule($module->id);
echo '<div class="row">';
foreach ($data as $parameterId => $parameter) {
    $htmlOptions = array_merge([
        'help' => '',
        'popover' => $parameter['help']
    ], $moduleInstance->getHtmlOptions($parameter));
    switch ($parameter['type']) {
        case 'text':
            echo '<div class="col-xs-12 col-md-6">';
            echo $form->field($model, $parameter['attribute'], $htmlOptions)->textInput([]);
            echo '</div>';
            break;
        case 'select':
            echo '<div class="col-xs-12 col-md-6">';
            echo $form->field($model, $parameter['attribute'], $htmlOptions)->widget(Select2::classname(), $moduleInstance->getSelect2Options($parameter));
            echo '</div>';
            break;
        case 'textarea':
            echo '<div class="col-12">';
            echo $form->field($model, $parameter['attribute'], $htmlOptions)->widget(CKEditor::className(), $moduleInstance->getWidgetOptions($parameter));
            echo '</div>';
            break;
        case 'radio':
            echo '<div class="col-xs-12 col-md-6">';
            echo $form->field($model, $parameter['attribute'], $htmlOptions)->radioList($moduleInstance->getDataRadio($parameter));
            echo '</div>';
            break;
        case 'date':
            echo '<div class="col-xs-12 col-md-6">';
            echo $form->field($model, $parameter['attribute'], $htmlOptions)->widget(DatePicker::classname(), $moduleInstance->getWidgetOptions($parameter));
            echo '</div>';
            break;
        case 'numeric':
            echo '<div class="col-xs-12 col-md-6">';
            echo $form->field($model, $parameter['attribute'], $htmlOptions)->widget(NumberControl::classname(), $moduleInstance->getWidgetOptions($parameter));
            echo '</div>';
            break;
        case 'time':
            echo '<div class="col-xs-12 col-md-6">';
            echo $form->field($model, $parameter['attribute'], $htmlOptions)->widget(TimePicker::classname(), $moduleInstance->getWidgetOptions($parameter));
            echo '</div>';
            break;
        case 'datetime':
            echo '<div class="col-xs-12 col-md-6">';
            echo $form->field($model, $parameter['attribute'], $htmlOptions)->widget(DateTimePicker::classname(), $moduleInstance->getWidgetOptions($parameter));
            echo '</div>';
            break;
        default:
            break;
    }

}
echo '</div>';