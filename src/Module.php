<?php

namespace ticmakers\parameters;

use Yii;
use ticmakers\core\base\Module as ModuleCore;
use ticmakers\core\i18n\LabelDbMessageSource;

/**
 * parameters module definition class
 *
 * @author Juan Sebastian Muñoz Reyes <juan.munoz@ticmakers.com>
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @author Kevin Daniel Guzman Delgadillo <kevin.guzman@ticmakers.com>
 */
class Module extends ModuleCore
{
    /**
     * @inheritDoc
     */
    public $defaultRoute = 'manage';
    /**
     * Proveedor de datos de la aplicación
     *
     * @var string
     */
    public $provider = 'ticmakers\parameters\helpers\GlobalProvider';

    /**
     * Permite establecer el nombre de la variable que almacena el identificador de la entidad en sesión: Yii::$app->session->get($module->nameSessionEntity)
     *
     * @var int
     */
    public $nameSessionEntity;
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'ticmakers\parameters\controllers';


    /**
     * Undocumented function
     *
     * @return void
     */
    public function init()
    {
        parent::init();
        Yii::createObject(\ticmakers\parameters\modules\api\Bootstrap::class)->bootstrap($this);
    }

    public function registerTranslations()
    {
        Yii::$app->i18n->translations["{$this->id}-labels*"] = [
            'class' => LabelDbMessageSource::class,
        ];
        parent::registerTranslations();
    }

    /**
     * Retorna las opciones para el widget ckeditor
     *
     * @return array
     */
    public function getHtmlOptions($dataParameters)
    {
        $options = (!empty($dataParameters['html_options'])) ? json_decode($dataParameters['html_options'], true) : [];
        return (isset($options['html_options'])) ? $options['html_options'] : [];
    }

    /**
     * Retorna las opciones para los widgets según lo guardado en la tabla
     *
     * @return array
     */
    public function getWidgetOptions($dataParameters)
    {
        $options = (!empty($dataParameters['html_options'])) ? json_decode($dataParameters['html_options'], true) : [];
        return (isset($options['widget_options'])) ? $options['widget_options'] : [];
    }

    /**
     * Retorna los datos del proveedor según el campo items
     *
     * @return array
     */
    public function getItems($dataParameters)
    {
        return (method_exists($this->provider, $dataParameters['items'])) ? call_user_func($this->provider . '::' . $dataParameters['items']) : [];
    }

    /**
     * Retorna las opciones para el widget select2
     *
     * @return array
     */
    public function getSelect2Options($dataParameters)
    {
        $options = $this->getWidgetOptions($dataParameters);
        $data = $this->getItems($dataParameters);
        return array_merge([
            'data' => $data,
        ], $options);
    }

    /**
     * Retorna las opciones para el elemento radio
     *
     * @return array
     */
    public function getDataRadio($dataParameters)
    {
        return $this->getItems($dataParameters);
    }

    /**
     * Permite obtener el valor para un parámetro del sistema
     *
     * @param string $code Código del parámetro del cual se desea obtener su valor
     * @return mixed
     */
    public function getParameterByCode($code, $entity = null)
    {
        $result = null;
        $params = [':code' => $code];
        $sql = "
            select par.parameter_id, par.code, par.transversal, pva.value, pva.entity_id
            from parameters par
            left join parameter_values pva ON par.parameter_id = pva.parameter_id AND pva.active = 'Y'
            where par.active = 'Y' AND par.code = :code
        ";
        if (!empty($entity)) {
            $sql .= " AND (pva.entity_id =:entity OR pva.entity_id IS NULL AND par.transversal = 'Y') ";
            $params[':entity'] = $entity;
        } else {
            $sql .= ' AND pva.entity_id IS NULL ';
        }
        $data = Yii::$app
            ->db
            ->createCommand($sql, $params)
            ->queryOne();
        if ($data) {
            $result = $data['value'];
        }
        return $result;
    }
}
