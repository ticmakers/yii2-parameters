<?php

namespace ticmakers\parameters\controllers;

use Yii;
use ticmakers\core\base\Controller;

/**
* Controlador CategoriesController implementa las acciones para el CRUD de el modelo ParameterCategories.
*
* @package ticmakers\parameters\controllers 
*
* @property string $modelClass Ruta del modelo principal.
* @property string $searchModelClass Ruta del modelo para la búsqueda.
*
* @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
* @copyright Copyright (c) 2019 TIC Makers S.A.S. 
* @version 0.0.1
* @since 1.0.0
*/
class CategoriesController extends Controller
{
    public $isModal = true;
    public $modelClass = \ticmakers\parameters\models\app\ParameterCategories::class;
    public $searchModelClass = \ticmakers\parameters\models\searchs\ParameterCategories::class;
}