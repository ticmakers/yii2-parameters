<?php

namespace ticmakers\parameters\controllers;

use Yii;
use ticmakers\core\base\Controller;

/**
 * Controlador ParameterValuesController implementa las acciones para el CRUD de el modelo ParameterValues.
 *
 * @package ticmakers\parameters\controllers 
 *
 * @property string $modelClass Ruta del modelo principal.
 * @property string $searchModelClass Ruta del modelo para la búsqueda.
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019  
 * @version 0.0.1
 * @since 1.0.0
 */
class ValuesController extends Controller
{
    public $isModal = true;
    public $modelClass = \ticmakers\parameters\models\app\ParameterValues::class;
    public $searchModelClass = \ticmakers\parameters\models\searchs\ParameterValues::class;
}
