<?php

namespace ticmakers\parameters\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use ticmakers\parameters\models\app\ParameterValues as ParameterValuesModel;

/**
 * Esta clase representa las búsqueda para el modelo `ticmakers\parameters\models\app\ParameterValues`.
 *
 * @package app
 * @subpackage models/searchs
 * @category Models
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019  
 * @version 0.0.1
 * @since 1.0.0
 */
class ParameterValues extends ParameterValuesModel
{
    /**
     * Define las reglas de validación de los datos.
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['parameter_value_id', 'parameter_id', 'entity_id', 'created_by', 'updated_by'], 'integer'],
            [['value', 'active', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * Escenarios del Modelo
     *
     * @return array
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Crea una instancia de un provider de datos con el query de búsqueda aplicado
     *
     * @param array $params Parametros para la búsqueda
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = static::find();

        //Agrega condiciones que quieras aplicar siempre aquí

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        //Condición para filtros
        $query->andFilterWhere([
            'parameter_value_id' => $this->parameter_value_id,
            'parameter_id' => $this->parameter_id,
            'entity_id' => $this->entity_id,
            'created_by' => $this->created_by,
            'DATE(created_at)' => $this->created_at,
            'updated_by' => $this->updated_by,
            'DATE(updated_at)' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'value', $this->value])
            ->andFilterWhere(['ilike', 'active', $this->active]);

        $dataProvider->sort->defaultOrder = [static::getNameFromRelations() => SORT_ASC];

        return $dataProvider;
    }
}
