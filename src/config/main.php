<?php
use yii\web\UrlManager;

$params = require __DIR__ . '/params.php';

$config = [
    'components' => [
        // lista de configuraciones de componente
    ],
    'params' => $params,
];

return $config;
