<?php

namespace ticmakers\parameters\modules\api\models\base;

use Yii;

/**
 * Éste es el modelo para la tabla "parameter_values".
 * Parameter's values
 *
 * @package app
 * @subpackage models/base
 * @category models
 *
 * @property integer $parameter_value_id Records's unique identifier
 * @property integer $parameter_id Parameter to which the value corresponds
 * @property integer $entity_id Entity to which the parameter belongs
 * @property string $value Parameter´s value
 * @property string $active Indicates whether the record is active or not
 * @property integer $created_by User's id who created the record
 * @property string $created_at Fecha y hora en que se creó el registro
 * @property integer $updated_by Last user's id who modified the record
 * @property string $updated_at Date and time of the last modification of the record
 * @property Parameters $parameter Datos relacionados con modelo "Parameters"
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 Tic Makers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class ParameterValues extends \ticmakers\parameters\models\base\ParameterValues
{

    /**
     * Undocumented function
     *
     * @return void
     */
    public function fields()
    {
        $fields = parent::fields();
        unset($fields['created_at'], $fields['updated_at']);
        $fields['code']= function($model){
            return $model->parameter->code;
        };
        return $fields;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function extraFields()
    {
        return [];
    }
}
