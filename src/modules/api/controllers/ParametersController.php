<?php

namespace ticmakers\parameters\modules\api\controllers;

use ticmakers\core\rest\ActiveController;

/**
 * UserController Clase encargada de presentar y manipular la información del modelo User para las solicitudes en el api
 *
 * @package app/modules
 * @subpackage rest/controllers
 * @category Controllers
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 
 * @copyright (c) 2019, TIC Makers S.A.S
 * @version 0.0.1
 */
class ParametersController extends ActiveController
{

    /**
     * Modelo para las operaciones CRUD
     * @var string
     */
    public $modelClass = \ticmakers\parameters\modules\api\models\base\Parameters::class;

    /**
     * Modelo para las búsquedas
     * @var string
     */
    public $searchModel = \ticmakers\parameters\modules\api\models\searchs\Parameters::class;

    /**
     * Llave primaria del modelo para la sincronización
     * @var string
     */
    public $primaryKey = 'parameter_id';
}
