<?php

use yii\web\UrlManager;

$params = require __DIR__ . '/params.php';

return [
    'components' => [
        'urlManager' => [
            'class' => UrlManager::class,
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => [
                        'parameters/api/parameter-categories',
                        'parameters/api/parameters',
                        'parameters/api/parameter-values',
                        ]
                ],
                'parameters/api/parameter-values/<code:[A-Z_0-9]+>' => 'parameters/api/parameter-values/view'
            ],
        ]
    ],
    'params' => $params,

];
