<?php
namespace ticmakers\parameters\migrations;
class m190213_160103_ParametersValues extends \yii\db\Migration
{
    public function up()
    {
        $tableName = 'parameter_values';
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($tableName, [
            'parameter_value_id' => $this->primaryKey(),
            'parameter_id' => $this->integer()->notNull(),
            'entity_id' => $this->integer(),
            'value' => $this->text()->notNull(),
            'active' => $this->char(1)->notNull(),
            'created_by' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'FOREIGN KEY ([[parameter_id]]) REFERENCES parameters ([[parameter_id]]) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);

        $this->addCommentOnColumn($tableName, 'parameter_value_id', "Records's unique identifier");
        $this->addCommentOnColumn($tableName, 'parameter_id', "Parameter to which the value corresponds");
        $this->addCommentOnColumn($tableName, 'entity_id', "Entity to which the parameter belongs");
        $this->addCommentOnColumn($tableName, 'value', "Parameter´s value");

        $this->addCommentOnColumn($tableName, 'active', "Indicates whether the record is active or not");
        $this->addCommentOnColumn($tableName, 'created_by', "User's id who created the record");
        $this->addCommentOnColumn($tableName, 'created_at', "Date and time the record was created");
        $this->addCommentOnColumn($tableName, 'updated_by', "Last user's id who modified the record");
        $this->addCommentOnColumn($tableName, 'updated_at', "Date and time of the last modification of the record");

        $this->createIndex('"idx-parameter_values.parameter_id"', $tableName, 'parameter_id');
        $this->execute("ALTER TABLE {$tableName} ADD CONSTRAINT \"chk-{$tableName}.active\"
                        CHECK (active IN ('Y', 'N'))");

    }

    public function down()
    {
        $tableName = 'parameter_values';
        $this->dropIndex($tableName, "idx-parameter_values.parameter_id");
        $this->execute("ALTER TABLE {$tableName} DROP CONSTRAINT \"fk-{$tableName}.parameter_id-parameters.parameter_id\"");
        $this->dropTable($tableName);
    }
}
