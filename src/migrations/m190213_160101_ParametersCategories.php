<?php
namespace ticmakers\parameters\migrations;
class m190213_160101_ParametersCategories extends \yii\db\Migration
{
    public $tableName = 'parameter_categories';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'parameter_category_id' => $this->primaryKey(),
            'name' => $this->text()->notNull(),
            'description' => $this->text(),
            'active' => $this->char(1)->notNull(),
            'created_by' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ], $tableOptions);

        $this->addCommentOnColumn($this->tableName, 'parameter_category_id', "Records's unique identifier");
        $this->addCommentOnColumn($this->tableName, 'name', "Category's name");
        $this->addCommentOnColumn($this->tableName, 'description', "Category's description");

        $this->addCommentOnColumn($this->tableName, 'active', "Indicates whether the record is active or not");
        $this->addCommentOnColumn($this->tableName, 'created_by', "User's id who created the record");
        $this->addCommentOnColumn($this->tableName, 'created_at', "Date and time the record was created");
        $this->addCommentOnColumn($this->tableName, 'updated_by', "Last user's id who modified the record");
        $this->addCommentOnColumn($this->tableName, 'updated_at', "Date and time of the last modification of the record");

        $this->execute("ALTER TABLE {$this->tableName} ADD CONSTRAINT \"chk-{$this->tableName}.active\"
                        CHECK (active IN ('Y', 'N'))");

    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
