<?php
namespace ticmakers\parameters\migrations;
class m190213_160102_Parameters extends \yii\db\Migration
{
    public $tableName = 'parameters';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tableName, [
            'parameter_id' => $this->primaryKey(),
            'parameter_category_id' => $this->integer()->notNull(),
            'code' => $this->string(128)->notNull(),
            'type' => $this->string(12)->notNull(),
            'html_options' => $this->text(),
            'label' => $this->text()->notNull(),
            'help' => $this->text()->notNull(),
            'order' => $this->smallInteger()->notNull()->defaultValue('0'),
            'items' => $this->text(),
            'mandatory' => $this->char(1)->notNull(),
            'transversal' => $this->char(1)->notNull(),
            'active' => $this->char(1)->notNull(),
            'created_by' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->notNull(),
            'updated_by' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            'FOREIGN KEY ([[parameter_category_id]]) REFERENCES parameter_categories ([[parameter_category_id]]) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);

        $this->addCommentOnColumn($this->tableName, 'parameter_id', "Records's unique identifier");
        $this->addCommentOnColumn($this->tableName, 'parameter_category_id', "Parameter's category");
        $this->addCommentOnColumn($this->tableName, 'code', "Parameter's code");
        $this->addCommentOnColumn($this->tableName, 'type', "Parameter´s type.  Can take the values: select, textarea, radio, date, numeric, text, time, datetime");
        $this->addCommentOnColumn($this->tableName, 'html_options', "Html options of the element");
        $this->addCommentOnColumn($this->tableName, 'label', "Parameter's label");
        $this->addCommentOnColumn($this->tableName, 'help', "Help text for parameter");
        $this->addCommentOnColumn($this->tableName, 'order', "Order for show the element");
        $this->addCommentOnColumn($this->tableName, 'items', "Items that can be selected as value");
        $this->addCommentOnColumn($this->tableName, 'mandatory', "The parameter is mandatory (Yes or No)");
        $this->addCommentOnColumn($this->tableName, 'transversal', "Parameter transversal to the system");

        $this->addCommentOnColumn($this->tableName, 'active', "Indicates whether the record is active or not");
        $this->addCommentOnColumn($this->tableName, 'created_by', "User's id who created the record");
        $this->addCommentOnColumn($this->tableName, 'created_at', "Date and time the record was created");
        $this->addCommentOnColumn($this->tableName, 'updated_by', "Last user's id who modified the record");
        $this->addCommentOnColumn($this->tableName, 'updated_at', "Date and time of the last modification of the record");

        $this->execute("ALTER TABLE {$this->tableName} ADD CONSTRAINT \"chk-{$this->tableName}.active\"
                        CHECK (active IN ('Y', 'N'))");
        $this->execute("ALTER TABLE {$this->tableName} ADD CONSTRAINT \"chk-{$this->tableName}.transversal\"
                        CHECK (transversal IN ('Y', 'N'))");
        $this->execute("ALTER TABLE {$this->tableName} ADD CONSTRAINT \"chk-{$this->tableName}.type\"
                        CHECK (type IN ('select', 'textarea', 'radio', 'date', 'numeric', 'text', 'time', 'datetime'))");

        $this->addCommentOnTable($this->tableName, "System's parameters");

    }

    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
