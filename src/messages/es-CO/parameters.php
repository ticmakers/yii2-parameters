<?php
return [
    'Name' => 'Nombre',
    'Description' => 'Descripción',
    'Parameter category id' => 'Código',
    'Parameter value id' => 'Código',
    'Parameter id' => 'Código',
    'Parameter Category' => 'Categoría de parámetro',
    'Code' => 'Código',
    'Type' => 'Tipo',
    'Html Options' => 'Opciones Html',
    'Label' => 'Etiqueta',
    'Help' => 'Ayuda',
    'Order' => 'Orden',
    'Items' => 'Items',
    'Mandatory' => 'Obligatorio',
    'Transversal' => 'Transversal',
    'Active' => 'Activo',
    'Created By' => 'Creado por',
    'Created At' => 'Fecha creación',
    'Updated By' => 'Modificado por',
    'Updated At' => 'Fecha actualización',
    'Parameter' => 'Parámetro',
    'Entity' => 'Entidad',
    'Value' => 'Valor',
    'Records\'s unique identifier' => 'Identificador único de los registros',
    'Parameter\'s category' => 'Categoría de parámetro',
    'Parameter\'s code' => 'Código de parámetros',
    'Html options of the element' => 'Opciones HTML del elemento',
    'Parameter\'s label' =>  'Etiqueta del parámetro',
    'Help text for parameter' => 'Texto de ayuda para el parámetro',
    'Order for show the element' =>  'Orden para mostrar el elemento',
    'Items that can be selected as value' => 'Elementos que pueden seleccionarse como valor',
    'The parameter is mandatory (Yes or No)' => 'El parámetro es obligatorio (Sí o No)',
    'Parameter transversal to the system' => 'Parámetro transversal al sistema',
    'Indicates whether the record is active or not' => 'Indica si el registro está activo o no',
    'User\'s id who created the record' => 'Id de usuario que creó el registro',
    'Date and time the record was created' =>  'Fecha y hora en que se creó el registro',
    'Last user\'s id who modified the record' => 'Id del último usuario que modificó el registro',
    'Date and time of the last modification of the record' => 'Fecha y hora de la última modificación del registro',
    'Parameter\'s type.  Can take the values: select, textarea, radio, date, numeric, text, time, datetime' => 'Tipo de parámetro. Puede tomar los valores: seleccionar, área de texto, radio, fecha, numérico, texto, hora, fecha y hora ',
    'Category\'s name' => 'Nombre de la categoría',
    'Category\'s description' => 'Descripción de la categoría',
    'Parameter to which the value corresponds' => 'Parámetro al que corresponde el valor',
    'Entity to which the parameter belongs' => 'Entidad a la que pertenece el parámetro',
    'Parameter´s value' => 'Valor del parámetro',
    'Edit parameters' => 'Editar parámetros',
    'Manage' => 'Administrar',
    'it was successfully edited' => 'Se editó con éxito.',
    'error when trying to edit' => 'Error al intentar editar.',
    'Manage' => 'Gestionar',
    'Edit {title}' => 'Editar {title}',
    'Parameters' => 'Parámetros',
    'Parameter categories' => 'Categorías de parámetros',
    'Parameter category' => 'Categoría de parámetros',
];
